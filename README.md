STEP Playground
=================

[![badge](https://img.shields.io/badge/master-playground-green)](https://termoshtt.gitlab.io/step-playground/)

This works on your browser using WebAssembly.

## Build

```
wasm-pack build --target web
```

This creates `./pkg/espr_playground.{js,wasm}`, and they are loaded from `./index.html`
