Container image of wasm-pack + Rust
------------------------------------

Configure is written in [compose.yml](./compose.yml) based on the [compose spec](https://github.com/compose-spec/compose-spec).

### Build

```
docker compose build
```

### Push

```
docker compose build
```

Currently, push of the container is done manually because it will be updated hardly.
